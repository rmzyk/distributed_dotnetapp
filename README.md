It is an application that I will build during course by Piotr Gankiewicz and Dariusz Pawlukiewicz. 
This is application written in .NET Core platform which implements basic distributed systems architecture also known as microservice architecture.
In this project I will implement following features:

-RESTful API implementation with ASP.NET Core
-Domain Driven Design fundamentals
-SQL and NoSQL databases (SQL Server, MongoDB, InfluxDB)
-Distributed caching with Redis
-API Gateway and other patterns designed for microservices
-JWT, authentication, authorization
-Communication via websockets using SignalR
-CQRS, Commands, Queries & Events handlers
-Using RabbitMQ as a message queue with RawRabbit
-Dealing with asynchronous requests, Process Managers and Sagas
-Internal HTTP communication with RestEase
-Service discovery with Consul
-Storing secrets with Vault
-Monitoring with App Metrics, Grafana, Prometheus and Jaeger
-Logging with Serilog, Seq and ELK stack
-Building Docker images, managing containers, networks and registries
-Defining Docker compose stacks
-Managing your own Nuget feeds (e.g. MyGet)
-CI & CD with build services such as Travis CI, Bitbucket Pipelines or VSTS
-Deploying services to the Linux Servers and configuring Nginx
-Orchestrating services on your VM or in the Cloud using Portainer or Rancher (built on top of Kubernetes)