﻿namespace DistributedShop.Common.Types.Base
{
    using System;
    using ITypes;

    public abstract class BaseEntity : IIdentifiable
    {
        public Guid Id { get; protected set; }

        public DateTime CreatedDate { get; protected set; }

        public DateTime UpdatedDate { get; protected set; }

        public BaseEntity(Guid id)
        {
            Id = id == Guid.Empty
                ? throw new DistributedShopException("Provided_invalid_id",
                    $"Provided id: {id} is invalid.")
                : id;

            CreatedDate = DateTime.UtcNow;
            SetUpdatedDate();
        }

        protected virtual void SetUpdatedDate()
            => UpdatedDate = DateTime.UtcNow;
    }
}
