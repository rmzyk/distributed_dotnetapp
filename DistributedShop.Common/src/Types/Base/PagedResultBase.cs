﻿namespace DistributedShop.Common.Types.Base
{
    public abstract class PagedResultBase
    {
        public int CurrentPage { get; }

        public int ResultsPerPage { get; }

        public int TotalPages { get; }

        public long TotalResults { get; }

        protected PagedResultBase() { }

        protected PagedResultBase(int currentPage, int resultsPage,
            int totalPages, long totalResults)
        {
            this.CurrentPage = currentPage > this.TotalPages ? totalPages : currentPage;
            this.ResultsPerPage = ResultsPerPage;
            this.TotalPages = totalPages;
            this.TotalResults = totalResults;
        }
    }
}
