﻿namespace DistributedShop.Common.Types.ITypes
{
    using System;

    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}
