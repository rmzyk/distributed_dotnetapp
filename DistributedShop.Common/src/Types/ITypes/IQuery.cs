﻿namespace DistributedShop.Common.Types.ITypes
{
    public interface IQuery
    { 
    }

    public interface IQuery<T> : IQuery
    {

    }
}
