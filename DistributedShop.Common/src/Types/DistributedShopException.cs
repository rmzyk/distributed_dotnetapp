﻿namespace DistributedShop.Common.Types
{
    using System;

    public class DistributedShopException : Exception
    {
        public string Code { get; }


        public DistributedShopException() { }

        public DistributedShopException(string code)
        {
            Code = code;
        }

        public DistributedShopException(string message, params object[] args)
            : this(string.Empty, message, args) { }

        public DistributedShopException(string code, string message, params object[] args)
            : this(null, code, message, args) { }

        public DistributedShopException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args) { }

        public DistributedShopException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = Code;
        }
    }
}
