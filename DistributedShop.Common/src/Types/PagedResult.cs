﻿namespace DistributedShop.Common.Types
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using Base;

    public class PagedResult<T> : PagedResultBase
    {
        public IEnumerable<T> Items { get; }

        public bool IsEmpty => this.Items is null || !this.Items.Any();

        public bool IsNotEmpty => !this.IsEmpty;

        public static PagedResult<T> Create(IEnumerable<T> items, int currentPage,
            int resultsPerPage, int totalPages, long totalResults)
            => new PagedResult<T>(items, resultsPerPage, currentPage, totalPages, totalResults);

        public static PagedResult<T> From(PagedResultBase result, IEnumerable<T> items)
            => new PagedResult<T>(items, result.CurrentPage, result.ResultsPerPage, result.TotalPages,
                result.TotalResults);

        public static PagedResult<T> Empty => new PagedResult<T>();

        protected PagedResult()
        {
            this.Items = Enumerable.Empty<T>();
        }

        [JsonConstructor]
        protected PagedResult(IEnumerable<T> items, int currentPage,
            int resultsPerPage, int totalPages, long totalResults) :
            base(currentPage, resultsPerPage, totalPages, totalResults)
        {
            this.Items = items;
        }
    }
}
