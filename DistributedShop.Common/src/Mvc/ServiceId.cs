﻿namespace DistributedShop.Common.Mvc
{
    using System;
    using IMvc;

    public class ServiceId : IServiceId
    {
        // https://fabiolb.net/
        private static readonly string UniqueId = $"{Guid.NewGuid():N}";

        public string Id => UniqueId;
    }
}
