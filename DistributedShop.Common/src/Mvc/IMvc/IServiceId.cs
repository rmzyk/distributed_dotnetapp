﻿namespace DistributedShop.Common.Mvc.IMvc
{
    public interface IServiceId
    {
        string Id { get; }
    }
}
