﻿namespace DistributedShop.Common
{
    using System.Threading.Tasks;

    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
