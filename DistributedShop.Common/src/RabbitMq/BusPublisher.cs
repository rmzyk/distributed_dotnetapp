﻿namespace DistributedShop.Common.RabbitMq
{
    using System.Threading.Tasks;
    using Messages.IMessages;
    using IRabbitMq;
    using RawRabbit;
    using RawRabbit.Enrichers.MessageContext;

    // https://rawrabbit.readthedocs.io/en/master/understanding-message-context.html#introduction
    public class BusPublisher : IBusPublisher
    {
        private readonly IBusClient _busClient;

        public BusPublisher(IBusClient busClient)
        {
            _busClient = busClient;
        }

        public async Task SendAsync<TCommand>(TCommand command, ICorrelationContext context) where TCommand : ICommand
            => await _busClient.PublishAsync(command, ctx => ctx.UseMessageContext(context));


        public async Task PublishAsync<TEvent>(TEvent @event, ICorrelationContext context) where TEvent : IEvent
            => await _busClient.PublishAsync(@event, ctx => ctx.UseMessageContext(context));
    }
}
