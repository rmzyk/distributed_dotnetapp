﻿namespace DistributedShop.Common.RabbitMq.IRabbitMq
{
    using System;
    using Messages.IMessages;
    using Types;


    public interface IBusSubscriber
   {
       IBusSubscriber SubscribeCommand<TCommand>(string @namespace = null, string queueName = null,
           Func<TCommand, DistributedShopException, IRejectedEvent> onError = null) where TCommand : ICommand;

       IBusSubscriber SubscribeEvent<TEvent>(string @namespace = null, string queueName = null,
           Func<TEvent, DistributedShopException, IRejectedEvent> onError = null) where TEvent : IEvent;
   }
}
