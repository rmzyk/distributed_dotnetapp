﻿namespace DistributedShop.Common.RabbitMq
{
    using System;
    using System.Threading.Tasks;
    using Handlers.IHandlers;
    using Messages;
    using Messages.IMessages;
    using IRabbitMq;
    using Types;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using OpenTracing;
    using OpenTracing.Tag;
    using Polly;
    // https://opentracing.io/docs/getting-started/
    using RawRabbit;
    using RawRabbit.Common;
    using RawRabbit.Enrichers.MessageContext;

    public class BusSubscriber : IBusSubscriber
    {
        private readonly ILogger _logger;
        private readonly IBusClient _busClient;
        private readonly IServiceProvider _serviceProvider;
        private readonly ITracer _tracer;
        private readonly int _retries;
        private readonly int _retryInterval;

        public BusSubscriber(IApplicationBuilder app)
        {
            _logger = app.ApplicationServices.GetService<ILogger<BusSubscriber>>();
            _serviceProvider = app.ApplicationServices.GetService<IServiceProvider>();
            _busClient = _serviceProvider.GetService<IBusClient>();
            _tracer = _serviceProvider.GetService<ITracer>();

            var options = _serviceProvider.GetService<RabbitMqOptions>();

            _retries = options.Retries >= 0 ? options.Retries : 3;
            _retryInterval = options.RetryInterval > 0 ? options.RetryInterval : 2;
        }

        public IBusSubscriber SubscribeCommand<TCommand>(string @namespace = null, string queueName = null,
            Func<TCommand, DistributedShopException, IRejectedEvent> onError = null) 
            where TCommand : ICommand
        {
            _busClient.SubscribeAsync<TCommand, CorrelationContext>(async (command, correlationContext) =>
            {
                var commandHandler = _serviceProvider.GetService<ICommandHandler<TCommand>>();

                return await TryHandleAsync(command, correlationContext,
                    () => commandHandler.HandleAsync(command, correlationContext), onError);
            });

            return this;
        }

        public IBusSubscriber SubscribeEvent<TEvent>(string @namespace = null, string queueName = null,
            Func<TEvent, DistributedShopException, IRejectedEvent> onError = null) 
            where TEvent : IEvent
        {
            _busClient.SubscribeAsync<TEvent, CorrelationContext>(async (@event, correlationContext) =>
            {
                var eventHandler = _serviceProvider.GetService<IEventHandler<TEvent>>();

                return await TryHandleAsync(@event, correlationContext,
                    () => eventHandler.HandleAsync(@event, correlationContext), onError);
            });

            return this;
        }

        private async Task<Acknowledgement> TryHandleAsync<TMessage>(TMessage message, CorrelationContext correlationContext,
            Func<Task> handle, Func<TMessage, DistributedShopException, IRejectedEvent> onError = null)
        {
            var currentRetry = 0;
            var retryPolicy = Policy.Handle<Exception>()
                .WaitAndRetryAsync(_retries, x => TimeSpan.FromSeconds(_retryInterval));

            var messageName = message.GetType().Name;

            return await retryPolicy.ExecuteAsync<Acknowledgement>(async () =>
            {
                var scope = _tracer
                    .BuildSpan("executing-handler")
                    .AsChildOf(_tracer.ActiveSpan)
                    .StartActive(true);

                using (scope)
                {
                    var span = scope.Span;

                    try
                    {
                        var retryMessage = currentRetry.Equals(0)
                            ? string.Empty
                            : $"Retry: {currentRetry}'.";

                        var preLogMessage = $"Handling a message: '{messageName}'" +
                                            $"with correlation id: '{correlationContext.Id}'. {retryMessage}";

                        _logger.LogInformation(preLogMessage);
                        span.Log(preLogMessage);

                        await handle();

                        var postLogMessage = $"Handled a message: '{messageName}'" +
                                             $"with correlation id: '{correlationContext.Id}'. {retryMessage}";
                        _logger.LogInformation(postLogMessage);
                        span.Log(postLogMessage);

                        return new Ack();
                    }
                    catch (Exception exception)
                    {
                        currentRetry++;
                        _logger.LogError(exception, exception.Message);
                        span.Log(exception.Message);
                        span.SetTag(Tags.Error, true);

                        if (exception is DistributedShopException distributedShopException && onError != null)
                        {
                            var rejectedEvent = onError(message, distributedShopException);
                            await _busClient.PublishAsync(rejectedEvent,
                                ctx => ctx.UseMessageContext(correlationContext));
                            _logger.LogInformation($"Published a rejected event: '{rejectedEvent.GetType().Name}'" +
                                                   $"for the message: '{messageName}' with correlation id: '{correlationContext.Id}'");

                            span.SetTag("error-type", "domain");
                            return new Ack();
                        }

                        span.SetTag("error-type", "infrastructure");
                        throw new Exception($"Unable to handle a message: '{messageName}'" +
                                            $"with correlation id: '{correlationContext.Id}'," +
                                            $"retry {currentRetry - 1}/{_retries}...");
                    }
                }
            });
        }

        private async Task<Acknowledgement> TryHandleWithRequeuingAsync<TMessage>(TMessage message,
            CorrelationContext correlationContext, Func<Task> handle,
            Func<TMessage, DistributedShopException, IRejectedEvent> onError = null)
        {
            var messageName = message.GetType().Name;
            var retryMessage = correlationContext.Retries.Equals(0)
                ? string.Empty
                : $"Retry: '{correlationContext.Retries}.'";

            _logger.LogInformation($"Handled a message: '{messageName}' " +
                                   $"with correlation id: '{correlationContext.Id}'. {retryMessage}");

            try
            {
                await handle();
                _logger.LogInformation($"Handled a message: '{messageName}' " +
                                       $"with correlation id: '{correlationContext.Id}'. {retryMessage}");
                // http://blogs.quovantis.com/rabbit-mq-working/

                return new Ack();
            }
            catch(Exception exception)
            {
                _logger.LogError(exception, exception.Message);
                if (exception is DistributedShopException distributedShopException && onError != null)
                {
                    var rejectedEvent = onError(message, distributedShopException);
                    await _busClient.PublishAsync(rejectedEvent, ctx => ctx.UseMessageContext(correlationContext));
                    _logger.LogInformation($"Published a rejected event: '{rejectedEvent.GetType().Name}'" +
                        $"for the message: '{messageName}' with correlation id: '{correlationContext.Id}'.");

                    return new Ack();
                }

                if (correlationContext.Retries >= _retries)
                {
                 await _busClient.PublishAsync(RejectedEvent.For(messageName),
                    ctx => ctx.UseMessageContext(correlationContext));

                throw new Exception($"Unable to handle a message: '{messageName}'" +
                                    $"with correlation id: '{correlationContext.Id}' +" +
                                    $"after {correlationContext.Retries} retries.", exception);
                }
                _logger.LogInformation($"Unable to handle a message: '{messageName}'" +
                                   $"with correlation id: '{correlationContext.Id}'," +
                                   $"retry {correlationContext.Retries}/{_retries}...");

            return Retry.In(TimeSpan.FromSeconds(_retryInterval));
            }
        }
    }
}
