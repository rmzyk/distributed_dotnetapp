﻿using DistributedShop.Common.Messages.IMessages;
using Newtonsoft.Json;

namespace DistributedShop.Common.Messages
{
    class RejectedEvent : IRejectedEvent
    {
        public string Reason { get; }

        public string Code { get; }

        // https://www.newtonsoft.com/json/help/html/JsonConstructorAttribute.htm
        [JsonConstructor]
        public RejectedEvent(string reason, string code)
        {
            Reason = reason;
            Code = code;
        }

        public static IRejectedEvent For(string name)
            => new RejectedEvent($"There was an error when executing: " + $"{name}", $"{name}_error");
    }
}
