﻿namespace DistributedShop.Common.Messages
{
    using System;

    // https://docs.microsoft.com/pl-pl/dotnet/csharp/programming-guide/concepts/attributes/attributeusage
    [AttributeUsage(AttributeTargets.Class)]
    public class MessageNamespaceAttribute : Attribute
    {
        public string Namespace { get; }

        public MessageNamespaceAttribute(string @namespace)
        {
            Namespace = @namespace?.ToLowerInvariant();
        }
    }
}
