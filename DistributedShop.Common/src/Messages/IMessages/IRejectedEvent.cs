﻿namespace DistributedShop.Common.Messages.IMessages
{
   public interface IRejectedEvent : IEvent
    {
        string Reason { get; }

        string Code { get; }
    }
}
