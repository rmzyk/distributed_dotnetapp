﻿namespace DistributedShop.Common.Messages.IMessages
{
    public interface IResource
    {
        Resource Resource { get; }
    }
}
