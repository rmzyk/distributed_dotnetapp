﻿namespace DistributedShop.Common.Dispatchers
{
    using System.Threading.Tasks;
    using Autofac;
    using IDispatchers;
    using Messages.IMessages;
    using RabbitMq;

    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly IComponentContext _context;

        public CommandDispatcher(IComponentContext context)
        {
            _context = context;
        }

        public Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand
                => await _context.Resolve<ICommandHandler<T>>().HandleAsync(command, CorrelationContext.Empty)
    }
}
