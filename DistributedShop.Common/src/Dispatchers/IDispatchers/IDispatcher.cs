﻿namespace DistributedShop.Common.Dispatchers.IDispatchers
{
    using System.Threading.Tasks;
    using Types.ITypes;
    using Messages.IMessages;

    public interface IDispatcher
    {
        Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand;

        Task<TResult> QueryAsync<TResult>(IQuery<TResult> query);
    }
}
