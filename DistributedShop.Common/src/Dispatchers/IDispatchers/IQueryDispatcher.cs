﻿namespace DistributedShop.Common.Dispatchers.IDispatchers
{
    using System.Threading.Tasks;
    using Types.ITypes;

    public interface IQueryDispatcher
   {
       Task<TResult> QueryAsync<TResult>(IQuery<TResult> query);
   }
}
