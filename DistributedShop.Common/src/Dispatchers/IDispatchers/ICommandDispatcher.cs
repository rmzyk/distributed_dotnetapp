﻿namespace DistributedShop.Common.Dispatchers.IDispatchers
{
    using System.Threading.Tasks;
    using Messages.IMessages;

    public interface ICommandDispatcher
    {
        Task SendAsync<TCommand>(TCommand command) where TCommand : ICommand;
    }
}
