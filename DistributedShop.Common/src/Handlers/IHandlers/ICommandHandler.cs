﻿namespace DistributedShop.Common.Handlers.IHandlers
{
    using System.Threading.Tasks;
    using Messages.IMessages;
    using RabbitMq.IRabbitMq;

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, ICorrelationContext context);
    }
}
