﻿namespace DistributedShop.Common.Handlers.IHandlers
{
    using System.Threading.Tasks;
    using Messages.IMessages;
    using RabbitMq.IRabbitMq;

    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        Task HandleAsync(TEvent @event, ICorrelationContext context);
    }
}
