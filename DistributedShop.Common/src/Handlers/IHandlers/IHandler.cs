﻿using System;
using System.Threading.Tasks;
using DistributedShop.Common.Types;

namespace DistributedShop.Common.Handlers.IHandlers
{
    public interface IHandler
    {
        IHandler Handle(Func<Task> handle);

        IHandler OnSuccess(Func<Task> onSuccess);

        IHandler OnError(Func<Exception, Task> onError, bool rethrow = false);

        IHandler OnCustomError(Func<DistributedShopException, Task> onCustomError, bool rethrow = false);

        IHandler Always(Func<Task> always);

        Task ExecuteAsync();
    }
}
