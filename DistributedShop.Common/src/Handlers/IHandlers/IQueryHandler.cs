﻿namespace DistributedShop.Common.Handlers.IHandlers
{
    using System.Threading.Tasks;
    using DistributedShop.Common.Types.ITypes;

    public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
    {
        Task<TResult> HandleAsync(TQuery query);
    }
}
