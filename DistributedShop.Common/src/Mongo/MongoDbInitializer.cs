﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace DistributedShop.Common.Mongo
{
    using System.Threading.Tasks;
    using IMongo;

    // https://info-mongodb-com.s3.us-east-1.amazonaws.com/MongoDB_Architecture_Guide.pdf
    // http://www.memonic.com/user/dorian/folder/internet-tidbits/id/1HiG
    // http://hebrayeem.blogspot.com/2014/01/making-sense-of-nosql.html
    public class MongoDbInitializer : IMongoDbInitializer
    {
        private static bool _isInitialized;
        private readonly bool _isSeeded;
        private readonly IMongoDatabase _database;
        private readonly IMongoDbSeeder _seeder;

        public MongoDbInitializer(IMongoDatabase database, IMongoDbSeeder seeder,
            MongoDbOptions options)
        {
            _database = database;
            _seeder = seeder;
            _isSeeded = options.Seed;
        }
        public async Task InitializeAsync()
        {
            if (_isInitialized)
            {
                return;
            }

            this.RegisterConventions();
            _isInitialized = true;

            if (!_isSeeded)
            {
                return;
            }

            await _seeder.SeedAsync();
        }

        private void RegisterConventions()
        {
           
            // https://www.mongodb.com/json-and-bson
            BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Decimal128));
            BsonSerializer.RegisterSerializer(typeof(decimal?), new NullableSerializer<decimal>(new DecimalSerializer(BsonType.Decimal128)));
            ConventionRegistry.Register("Conventions", new MongoDbConventions(), x => true);
        }

        private class MongoDbConventions : IConventionPack
        {
            public IEnumerable<IConvention> Conventions 
                => new List<IConvention>
                {
                    new IgnoreExtraElementsConvention(true),
                    new EnumRepresentationConvention(BsonType.String),
                    new CamelCaseElementNameConvention()
                };
        }
    }
}
