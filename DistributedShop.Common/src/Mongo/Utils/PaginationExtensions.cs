﻿namespace DistributedShop.Common.src.Mongo.Utils
{
    using System;
    using System.Threading.Tasks;
    using Types;
    using Types.Base;
    using MongoDB.Driver;
    using MongoDB.Driver.Linq;

    public static class PaginationExtensions
    {
        public static async Task<PagedResult<T>> PaginateAsync<T>(this IMongoQueryable<T> collection, PagedQueryBase query)
            => await collection.PaginateAsync(query.Page, query.Results);

        public static async Task<PagedResult<T>> PaginateAsync<T>(this IMongoQueryable<T> collection, int page = 1,
            int resultsPerPage = 10)
        {
            page = page <= 0 ? 1 : page;
            resultsPerPage = resultsPerPage <= 0 ? 10 : resultsPerPage;

            var isEmpty = await collection.AnyAsync() == false;

            if (isEmpty)
            {
                return PagedResult<T>.Empty;
            }

            var totalResults = await collection.CountAsync();
            var totalPages = (int) Math.Ceiling((decimal) totalResults / resultsPerPage);
            var data = await collection.Limit(page, resultsPerPage).ToListAsync();

            return PagedResult<T>.Create(data, page, resultsPerPage, totalPages, totalResults);
        }

        public static IMongoQueryable Limit<T>(this IMongoQueryable<T> collection, PagedQueryBase query)
            => collection.Limit(query.Page, query.Results);

        public static IMongoQueryable<T> Limit<T>(this IMongoQueryable<T> collection,
            int page = 1, int resultsPerPage = 10)
        {
            page = page <= 0 ? 1 : page;
            resultsPerPage = resultsPerPage <= 0 ? 10 : resultsPerPage;

            var skip = (page - 1) * resultsPerPage;
            var data = collection.Skip(skip).Take(resultsPerPage);

            return data;
        }
    }
}
