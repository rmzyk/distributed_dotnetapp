﻿namespace DistributedShop.Common.Mongo
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using IMongo;
    using Types;
    using Types.Base;
    using Types.ITypes;
    using DistributedShop.Common.src.Mongo.Utils;
    using MongoDB.Driver;
    using MongoDB.Driver.Linq;

    public class MongoDbRepository<TEntity> : IMongoDbRepository<TEntity> where TEntity : IIdentifiable
    {
        protected IMongoCollection<TEntity> Collection { get; }

        public MongoDbRepository(IMongoDatabase database, string collectionName)
        {
            this.Collection = database.GetCollection<TEntity>(collectionName);
        }

        public async Task<TEntity> GetAsync(Guid id)
            => await GetAsync(x => x.Id == id);

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
            => await this.Collection.Find(predicate).SingleOrDefaultAsync();

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
            => await this.Collection.Find(predicate).ToListAsync();

        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, TQuery query)
            where TQuery : PagedQueryBase
            => await this.Collection.AsQueryable().Where(predicate).PaginateAsync(query);

        public async Task AddAsync(TEntity entity)
            => await this.Collection.InsertOneAsync(entity);

        public async Task UpdateAsync(TEntity entity)
            => await Collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);

        public async Task DeleteAsync(Guid id)
            => await this.Collection.DeleteOneAsync(x => x.Id == id);

        public async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate)
            => await this.Collection.Find(predicate).AnyAsync();
    }
}
