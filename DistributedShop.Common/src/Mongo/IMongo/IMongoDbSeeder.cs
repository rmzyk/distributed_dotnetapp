﻿namespace DistributedShop.Common.Mongo.IMongo
{
    using System.Threading.Tasks;

    public interface IMongoDbSeeder
    {
        Task SeedAsync();
    }
}
