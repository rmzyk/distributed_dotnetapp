﻿namespace DistributedShop.Common.Mongo.IMongo
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Types;
    using Types.Base;
    using Types.ITypes;

    public interface IMongoDbRepository<TEntity> where TEntity : IIdentifiable
    {
        Task<TEntity> GetAsync(Guid id);

        // interesting part -> Expression is used to take description of delegate and properly use it
        // in linq-sql connection. More info on: 
        // https://stackoverflow.com/questions/793571/why-would-you-use-expressionfunct-rather-than-funct
        // https://www.tutorialsteacher.com/linq/linq-expression
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);

        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate,
            TQuery query) where TQuery : PagedQueryBase;

        Task AddAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteAsync(Guid id);

        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
