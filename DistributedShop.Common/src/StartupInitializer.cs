﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributedShop.Common
{
    public class StartupInitializer : IStartupInitializer
    {
        private readonly ISet<IInitializer> _initializers = new HashSet<IInitializer>();
        // https://github.com/ktaranov/naming-convention/blob/master/C%23%20Coding%20Standards%20and%20Naming%20Conventions.md

        public void AddInitializer(IInitializer initializer)
             => _initializers.Add(initializer);

        public async Task InitializeAsync()
            => await Task.WhenAll(_initializers.Select(x => x.InitializeAsync()));
    }
}
