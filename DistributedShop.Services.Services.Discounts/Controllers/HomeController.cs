﻿namespace DistributedShop.Services.Discounts.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult GeT()
            => Ok("Hi from Discount service.");
    }
}
