﻿namespace DistributedShop.Services.Discounts.Domain
{
    using Common.Types.ITypes;
    using Common.Types;
    using System;

    public class Discount : IIdentifiable
    {
        public Guid Id { get; private set; }

        public Guid CustomerId { get; private set; }

        public string Code { get; private set; }

        public double Percentage { get; private set; }

        public DateTime? UsedAt { get; private set; }


        private Discount() { }

        public Discount(Guid id, Guid customerId, string code,
            double percentage)
        {
            this.Id = id == Guid.Empty
                ? throw new DistributedShopException("invalid_discount_id",
                    "Improper discount id.")
                : id;

            this.CustomerId = customerId == Guid.Empty
                ? throw new DistributedShopException("invalid_customer_id",
                    "Improper discount's customer id.")
                : customerId;

            this.Code = string.IsNullOrWhiteSpace(code)
                ? throw new DistributedShopException("empty_discount_code",
                    "Empty discount code.")
                : code;

            this.Percentage = (percentage < 1 || percentage > 100)
                ? throw new DistributedShopException("invalid_discount_percentage",
                    $"Invalid discount percentage {percentage}.")
                : percentage;
        }

        public void Use()
        {
            this.UsedAt = this.UsedAt.HasValue
                ? throw new DistributedShopException("discount_already_applied",
                    $"Discount: '{this.Id}' was already used at: {this.UsedAt}.")
                : DateTime.UtcNow;
        }
    }
}

