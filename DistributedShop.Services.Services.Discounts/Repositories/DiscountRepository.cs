﻿using DistributedShop.Common.Mongo.IMongo;

namespace DistributedShop.Services.Discounts.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Domain;
    using IRepositories;

    public class DiscountRepository : IDiscountRepository
    {
        private readonly IMongoDbRepository<Discount> _repository;

        public DiscountRepository(IMongoDbRepository<Discount> repository)
        {
            _repository = repository;
        }

        public async Task AddASync(Discount discount)
            => await _repository.AddAsync(discount);

        public async Task UpdateAsync(Discount discount)
            => await _repository.UpdateAsync(discount);

        public async Task GetAsync(Guid id)
            => await _repository.GetAsync(id);
    }
}
