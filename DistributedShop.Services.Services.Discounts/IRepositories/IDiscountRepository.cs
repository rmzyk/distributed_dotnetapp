﻿namespace DistributedShop.Services.Discounts.IRepositories
{
    using System;
    using System.Threading.Tasks;
    using Domain;

    public interface IDiscountRepository
   {
       Task AddASync(Discount discount);

       Task UpdateAsync(Discount discount);

       Task GetAsync(Guid id);
   }
}
